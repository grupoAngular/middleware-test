"use strict";
var odata = require("odata");
var config = require("../config");
var clientData = require("../data/client")


var clients = clientData.getClients();

function find() {
  // simulamos una connection a bbdd y devolvemos la lista de clients, en este caso a services.js
 return clients;
}

module.exports = {
  find: find,
};
