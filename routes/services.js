("use strict");
var models = require("../model/client_model");

module.exports = {
  test: test,
};

function test(req, res) {
  // nos subscribimos a el method find de client_model.js para que nos devuelva una lista de clients
  var clients = models.find();

  // devolvemos los datos que hemos recogido
  res.status(200).send({
    message: "clients",
    data: clients,
  });
  next();
}
