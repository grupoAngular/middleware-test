to run the project:

- npm install
- node app.js

it should reply

'listening on port 3005'

to test:

open the sample urls:

http://localhost:3005/test

---

El hilo de ejecución es el siguiente:

- Lo primero en el app.js esta declarado todo, el puerto que vamos a usar, importamos express etc etc

- Por otra parte en la carpeta routers tenemos services.js, en este haremos los metodos, nos conectamos a client_model y desde este devolvemos una lista de clientes,

- En client_model la llamada a bbdd, pero haremos un ejercicio con esto y nos conectaremos a alguna bbdd para que devuelva datos.

- Una vez tengamos hecho el metodo, tenemos que ir al index.js y declarar la ruta que llamaremos desde front o desde el navegador mismo seria la url a consultar, asi nos conectamos a middleware o back end que en este caso es lo mismo.

- Para levantar el proyecto tienes que entrar desde consola en la carpeta middleware, es decir => cd middleware y una vez dentro debes escribir en consola node app.js (esto levanta el servidor).

- En el config.js (no lo vamos a usar de momento) se hacen las configuraciones para conectarte a bbdd, simplemente por externalizar datos y no mezclarlo todo.

HILO DE EJECUCIÓN
-------------- services.js (creas metodos) => index.js (declaras la ruta) -------------
